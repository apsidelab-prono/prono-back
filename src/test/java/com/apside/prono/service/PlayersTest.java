package com.apside.prono.service;

import com.apside.prono.errors.common.EntityNotFoundException;
import com.apside.prono.errors.player.BadRequestCreatePlayerException;
import com.apside.prono.mapper.player.PlayerEntityMapper;
import com.apside.prono.mapper.player.PlayerMapper;
import com.apside.prono.model.PlayerEntity;
import com.apside.prono.modelapi.Player;
import com.apside.prono.repository.PlayerRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.management.BadAttributeValueExpException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class PlayersTest {

    @Spy
    private PlayerMapper playerMapper = Mappers.getMapper(PlayerMapper.class);

    private static final String LIBELLE_PLAYER1 = "player1@player.com";
    private static final String LIBELLE_PLAYER2 = "player2@player.com";
    @Mock
    private PlayerRepository playerRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private Environment environment;
    @InjectMocks
    private PlayerService playerService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.playerService = new PlayerService(this.playerRepository, this.passwordEncoder, this.environment);
    }

    @Test
    public void testGetAllPlayers() {
        List<PlayerEntity> playerEntityList = new ArrayList<>();
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setId(1L);
        playerEntity.setMail(LIBELLE_PLAYER1);
        PlayerEntity playerEntity2 = new PlayerEntity();
        playerEntity2.setId(2L);
        playerEntity2.setMail(LIBELLE_PLAYER2);
        playerEntityList.add(playerEntity);
        playerEntityList.add(playerEntity2);

        when(this.playerRepository.findAll()).thenReturn(playerEntityList);

        List<Player> result = this.playerService.getAll();
        assertEquals(2, result.size());
    }

    @Test
    public void testGetOnePlayer() {
        Optional<PlayerEntity> playerEntity = Optional.of(new PlayerEntity());
        playerEntity.get().setId(1L);
        playerEntity.get().setMail(LIBELLE_PLAYER1);

        when(this.playerRepository.findById(1L)).thenReturn(playerEntity);
        Player result = this.playerService.getPlayer(1L);
        assertEquals(String.valueOf(1), result.getId());
        assertEquals(LIBELLE_PLAYER1, result.getMail());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testEntityNotFoundGetOnePlayer() throws Exception {
        this.playerService.getPlayer(1L);
    }

    @Test(expected = BadRequestCreatePlayerException.class)
    public void testBadRequestCreatePlayer() throws Exception {
        Player player = new Player();
        player.setId("1");
        this.playerService.createPlayer(player);
    }

    @Test
    public void testSavePlayer() {
        Player player = new Player();
        player.setMail(LIBELLE_PLAYER1);
        PlayerEntity playerSave = new PlayerEntity();
        playerSave.setId(1L);
        playerSave.setMail(LIBELLE_PLAYER1);

        when(this.playerRepository.save(PlayerEntityMapper.INSTANCE.mapPlayerEntity(player))).thenReturn(playerSave);

        PlayerEntity player1 = this.playerService.createPlayer(player);

        assertEquals(Long.valueOf(1L), player1.getId());
        assertEquals(LIBELLE_PLAYER1, player1.getMail());
    }


    @Test
    public void testUpdatePlayer() {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setMail(LIBELLE_PLAYER1);
        playerEntity.setId(1L);
        Player playerUpdate = new Player();
        playerUpdate.setId("1");
        playerUpdate.setMail(LIBELLE_PLAYER2);
        when(this.playerRepository.findById(1L)).thenReturn(Optional.of(PlayerEntityMapper.INSTANCE.mapPlayerEntity(playerUpdate)));

        PlayerEntity player = this.playerService.update(playerUpdate);

        assertEquals(Long.valueOf(1L), player.getId());
        assertEquals(LIBELLE_PLAYER2, player.getMail());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testEntityNotFoundUpdatePlayer() throws Exception {
        Player player = new Player();
        player.setId("1");
        player.setMail(LIBELLE_PLAYER2);
        this.playerService.update(player);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testEntityNotFoundUpdatePlayerEmpty() throws Exception {
        Player player = null;
        this.playerService.update(player);
    }

    @Test(expected = BadRequestCreatePlayerException.class)
    public void testEntityNotFoundCreatePlayerEmpty() throws Exception {
        Player player = null;
        this.playerService.createPlayer(player);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testBadRequestDeletePlayer() throws Exception {
        this.playerService.delete(-1L);
    }

    @Test
    public void testDeletePlayer() {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setId(1L);
        playerEntity.setMail(LIBELLE_PLAYER1);
        PlayerEntity playerEntity2 = new PlayerEntity();
        playerEntity2.setId(2L);
        playerEntity2.setMail(LIBELLE_PLAYER1);

        when(this.playerRepository.findById(1L)).thenReturn(Optional.of(playerEntity2));

        playerService.delete((playerEntity.getId()));

        verify(this.playerRepository, times(1)).deleteById(playerEntity.getId());

    }

    @Test
    public void testFindPlayerByPseudo() {
        Optional<PlayerEntity> playerEntity = Optional.of(new PlayerEntity());
        playerEntity.get().setId(1L);
        playerEntity.get().setPseudo(LIBELLE_PLAYER1);

        when(this.playerRepository.findByPseudo(LIBELLE_PLAYER1)).thenReturn(playerEntity);

        Player player1 = this.playerService.getPlayerByPseudo(LIBELLE_PLAYER1);

        assertEquals(LIBELLE_PLAYER1, player1.getPseudo());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testFindPlayerByPseudoNotPresent() {
    	this.playerService.getPlayerByPseudo(LIBELLE_PLAYER1);
    }



    @Test
    public void testFindPlayerByLabel() {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setId(1L);
        playerEntity.setMail(LIBELLE_PLAYER1);
        List<PlayerEntity> list = new ArrayList<>();
        list.add(playerEntity);
        when(this.playerRepository.findAll()).thenReturn(list);
        assertEquals(playerEntity.getMail(), this.playerService.find(LIBELLE_PLAYER1).getMail());
    }

    @Test
    public void testFindPlayerByLabelResultNull() {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setId(1L);
        playerEntity.setMail(LIBELLE_PLAYER1);

        assertEquals(null, this.playerService.find(LIBELLE_PLAYER1));
    }

    @Test
    public void testExistByPseudo() {
    	PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setId(1L);
        playerEntity.setPseudo(LIBELLE_PLAYER1);

        when(this.playerRepository.existsPlayerEntitiesByPseudo(LIBELLE_PLAYER1)).thenReturn(true);

        Boolean test = this.playerService.existsByUsername(LIBELLE_PLAYER1);

        assertTrue(test);
    }

    @Test
    public void testExistByMail() {
    	PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setId(1L);
        playerEntity.setMail(LIBELLE_PLAYER1);

        when(this.playerRepository.existsPlayerEntitiesByMail(LIBELLE_PLAYER1)).thenReturn(true);

        Boolean test = this.playerService.existsByEmail(LIBELLE_PLAYER1);

        assertTrue(test);
    }


}
