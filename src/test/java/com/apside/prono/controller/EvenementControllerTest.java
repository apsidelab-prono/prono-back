package com.apside.prono.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.apside.prono.model.EvenementEntity;
import com.apside.prono.service.EvenementService;

@RunWith(SpringJUnit4ClassRunner.class)
public class EvenementControllerTest {
	
    private static final String LIBELLE_EVENT1 = "evenement 1";
    private static final String LIBELLE_EVENT2 = "evenement 2";
    
    @Mock
    EvenementService evenementService;
    
    @InjectMocks
    EvenementController evenementController;
    
    @Before
    public void setUp() {
    	
    MockitoAnnotations.initMocks(this);
    
    }

	@Test
	public void getAllEvenementController() {
		
        List<EvenementEntity> evenementEntityList = new ArrayList<>();
        EvenementEntity evenementEntity = new EvenementEntity();
        evenementEntity.setId(1L);
        evenementEntity.setLibelle(LIBELLE_EVENT1);
        EvenementEntity evenementEntity2 = new EvenementEntity();
        evenementEntity2.setId(2L);
        evenementEntity2.setLibelle(LIBELLE_EVENT2);
        evenementEntityList.add(evenementEntity);
        evenementEntityList.add(evenementEntity2);
        
        when(evenementService.getAll()).thenReturn(evenementEntityList);
        
        List<EvenementEntity> result = evenementController.getAllEvenements();
        
        assertEquals(2, result.size());
	}

}
