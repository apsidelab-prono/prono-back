package com.apside.prono.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/** 

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PlayerControllerTestMVC {
	
	@Autowired
	private MockMvc mvc;

	 

	@Test
	public void testGetAllPlayersAPI() throws Exception {
		
		mvc.perform(MockMvcRequestBuilders
				.get("/players")
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.players").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.players[*].id").isNotEmpty());
				
	//	Status 401 Unauthorized

	}
	
	@Test
	public void testGetPlayersByIdAPI() throws Exception {
		
		mvc.perform(MockMvcRequestBuilders
				.get("/players/{id}", 1)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
				
	//	Status 401 Unauthorized

	}
	


}

**/
