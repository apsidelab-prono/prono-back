package com.apside.prono.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.apside.prono.model.ActorEntity;
import com.apside.prono.model.EvenementEntity;
import com.apside.prono.model.PlayerEntity;
import com.apside.prono.model.PronoEntity;
import com.apside.prono.model.PronoPlayerEntity;
import com.apside.prono.model.TypeEntity;
import com.apside.prono.service.PronoPlayerService;

@RunWith(SpringJUnit4ClassRunner.class)
public class PronoPlayerControllerTest {
	
    String lastName = "Monkey D.";
    String firstName = "Luffy";
    String mail = "one@piece.jp";
    Date subscribeDate = new Date();
    
    @InjectMocks
    private PronoPlayerController pronoPlayerController;
    
    @Mock
    private PronoPlayerService pronoPlayerService;
    private Long value = 5L;
    private String label = "testPronoPlayer";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
	
	
	public PronoPlayerEntity initialize() {
        TypeEntity typeEntity = new TypeEntity();
        typeEntity.setLabel(label);
        typeEntity.setId(1L);

        ActorEntity actorEntity = new ActorEntity();
        actorEntity.setLabel(label);
        actorEntity.setId(1L);

        EvenementEntity eventEntity = new EvenementEntity();
        eventEntity.setLibelle(label);
        eventEntity.setCoeff(value);
        eventEntity.setDateEvenement(new Date());
        eventEntity.setDateFermeture(new Date());
        eventEntity.setDateOuverture(new Date());
        eventEntity.setId(1L);
        eventEntity.setId(1L);

        PlayerEntity player = new PlayerEntity();
        player.setFirstName(firstName);
        player.setLastName(lastName);
        player.setMail(mail);
        player.setSubscribeDate(subscribeDate);
        player.setId(1L);

        PronoEntity pronoEntity = new PronoEntity();
        pronoEntity.setScore(String.valueOf(value));
        pronoEntity.setId(1L);


        PronoPlayerEntity pronoPlayerEntity = new PronoPlayerEntity();
        pronoPlayerEntity.setType(typeEntity);
        pronoPlayerEntity.setActor(actorEntity);
        pronoPlayerEntity.setEvent(eventEntity);
        pronoPlayerEntity.setPlayer(player);
        pronoPlayerEntity.setPronoDetail(pronoEntity);
        pronoPlayerEntity.setDate(new Date());
        return pronoPlayerEntity;
    }

	@Test
	public void getAllPronoPlayerController() {
		
        List<PronoPlayerEntity> pronoPlayerEntityList = new ArrayList<>();
        PronoPlayerEntity pronoPlayerEntity = initialize();
        PronoPlayerEntity pronoPlayerEntity2 = initialize();
        pronoPlayerEntityList.add(pronoPlayerEntity);
        pronoPlayerEntityList.add(pronoPlayerEntity2);
        
        when(pronoPlayerService.getAll()).thenReturn(pronoPlayerEntityList);

        List<PronoPlayerEntity> result = pronoPlayerController.getAllPronoPlayer();
        
        assertEquals(2, result.size());

	}

}
