package com.apside.prono.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.apside.prono.model.PlayerEntity;
import com.apside.prono.modelapi.Player;
import com.apside.prono.service.PlayerService;

@RunWith(SpringJUnit4ClassRunner.class)
public class PlayerControllerTest {

    private static final String LIBELLE_PLAYER1 = "player1@player.com";
    private static final String LIBELLE_PLAYER2 = "player2@player.com";

	@InjectMocks
	PlayerController playerController;
    @Mock
	PlayerService playerService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private Environment environment;
    

    @Before
    public void setUp() {

    MockitoAnnotations.initMocks(this);
    this.playerController = new PlayerController(this.playerService, this.passwordEncoder, this.environment);

    }


	@Test
	public void getAllPlayerController() {

        List<Player> playerList = new ArrayList<>();
        Player player = new Player();
        player.setId("1");
        player.setMail(LIBELLE_PLAYER1);
        Player player2 = new Player();
        player2.setId("2");
        player2.setMail(LIBELLE_PLAYER2);
        playerList.add(player);
        playerList.add(player2);

        when(this.playerService.getAll()).thenReturn(playerList);

        List<Player> result = this.playerController.getAllPlayers();

        assertEquals(2, result.size());
        verify(this.playerService, times(1)).getAll();

	}

	@Test
	public void getPlayerByIdController() {

        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Player player = new Player();
        player.setFirstName("Jeanne");
        player.setLastName("Logonout");
        player.setMail(LIBELLE_PLAYER1);
        player.setSubscribeDate(new Date());

        when(this.playerService.getPlayer(1L)).thenReturn(player);

        ResponseEntity<?> result = this.playerController.getPlayer(1L);

        assertEquals(player, result.getBody());
        verify(this.playerService, times(1)).getPlayer(1L);

	}

	@Test
	public void deletePlayerController() {

		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Player player = new Player();
        player.setFirstName("Jeanne");
        player.setLastName("Logonout");
        player.setMail(LIBELLE_PLAYER1);
        player.setSubscribeDate(new Date());
        
        ResponseEntity<?> result = this.playerController.deletePlayer(1L);
        
        assertEquals(null, result.getBody());
        verify(this.playerService, times(1)).delete(1L);
	}
	
	@Test
	public void createPlayerController() throws URISyntaxException {
		
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        
        Player player = new Player();
        player.setFirstName("Jeanne");
        player.setLastName("Logonout");
        player.setMail(LIBELLE_PLAYER1);
        player.setSubscribeDate(new Date());
        
        ResponseEntity<?> result = this.playerController.createPlayer(player);
        
        assertEquals(player, result.getBody());
        verify(this.playerService, times(1)).createPlayer(player);
        
	}
	
	@Test
	public void savePlayerController() throws URISyntaxException {
		
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        
        Player player = new Player();
        player.setId("1");
        player.setFirstName("Jeanne");
        player.setLastName("Logonout");
        player.setMail(LIBELLE_PLAYER1);
        player.setSubscribeDate(new Date());
        
        Player player1 = new Player();
        player1.setId("1");
        player1.setFirstName("Jeanne");
        player1.setLastName("Logonout");
        player1.setMail(LIBELLE_PLAYER2);
        player1.setSubscribeDate(new Date());
        
        ResponseEntity<?> result = this.playerController.updatePlayer(player1);
        
        assertEquals(player1, result.getBody());
        verify(this.playerService, times(1)).update(player1);
        
	}

}
