package com.apside.prono.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.apside.prono.model.ResultEntity;
import com.apside.prono.service.ResultService;

@RunWith(SpringJUnit4ClassRunner.class)
public class ResultControllerTest {
	
    private static final String LIBELLE_RESULT1 = "result 1";
    private static final String LIBELLE_RESULT2 = "result 2";
    
	@InjectMocks
	ResultController resultController;
    
    @Mock
	ResultService resultService;
    
    @Before
    public void setUp() {
    	
    MockitoAnnotations.initMocks(this);
    
    }

	@Test
	public void getAllResultController() {
		
        List<ResultEntity> resultEntityList = new ArrayList<>();
        ResultEntity resultEntity = new ResultEntity();
        resultEntity.setId(1L);
        resultEntity.setResult(LIBELLE_RESULT1);
        ResultEntity resultEntity2 = new ResultEntity();
        resultEntity2.setId(2L);
        resultEntity2.setResult(LIBELLE_RESULT2);
        resultEntityList.add(resultEntity);
        resultEntityList.add(resultEntity2);
        
        when(resultService.getAll()).thenReturn(resultEntityList);
        
        List<ResultEntity> result = resultController.getAllResult();
        
        assertEquals(2, result.size());
		
	}

}
