package com.apside.prono.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import com.apside.prono.model.PronoEntity;
import com.apside.prono.service.PronoService;

@RunWith(SpringJUnit4ClassRunner.class)
public class PronoControllerTest {
	
	private static final String LIBELLE_PRONO1 = "prono 1";
    private static final String LIBELLE_PRONO2 = "prono 2";
    
    @Mock
    PronoService pronoService;
    
    @InjectMocks
    PronoController pronoController;
    
    @Before
    public void setUp() {
    	
    MockitoAnnotations.initMocks(this);
    
    }

	@Test
	public void getAllPronoController() {
		
		List<PronoEntity> pronoEntityList = new ArrayList<>();
		PronoEntity pronoEntity = new PronoEntity();
		pronoEntity.setId(1L);
		pronoEntity.setScore(LIBELLE_PRONO1);
		
		PronoEntity pronoEntity2 = new PronoEntity();
		pronoEntity2.setId(2L);
		pronoEntity2.setScore(LIBELLE_PRONO2);
		pronoEntityList.add(pronoEntity);
		pronoEntityList.add(pronoEntity);
		
		when(pronoService.getAll()).thenReturn(pronoEntityList);
		
		List<PronoEntity> result = pronoController.getAllProno();
		
		assertEquals(2, result.size());
		
	}
}
