package com.apside.prono.controller;

import com.apside.prono.model.PlayerEntity;
import com.apside.prono.securityconfig.controller.AuthRestAPIs;
import com.apside.prono.securityconfig.jwt.JwtProvider;
import com.apside.prono.securityconfig.message.request.LoginForm;
import com.apside.prono.securityconfig.message.response.JwtResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthRestAPIsTest {
    private static final String MAIL_PLAYER1 = "player1@player.com";
    private static final String PSEUDO_PLAYER1 = "player1";
    private static final String PASSWORD_PLAYER1 = "password1";
    private static final String FIRSTNAME_PLAYER1 = "firstname1";
    private static final String LASTNAME_PLAYER1 = "lastname1";

    @InjectMocks
    AuthRestAPIs authRestAPIs;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    JwtProvider jwtProvider;

    @Mock
    Authentication authentication;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAuthenticateUser() {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setMail(MAIL_PLAYER1);
        playerEntity.setPseudo(PSEUDO_PLAYER1);
        playerEntity.setFirstName(FIRSTNAME_PLAYER1);
        playerEntity.setLastName(LASTNAME_PLAYER1);
        playerEntity.setPassword(PASSWORD_PLAYER1);
        playerEntity.setId(1L);

        LoginForm loginRequest = new LoginForm();
        loginRequest.setUsername(playerEntity.getPseudo());
        loginRequest.setEmail(playerEntity.getMail());
        loginRequest.setPassword(playerEntity.getPassword());
        String jwt = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0b3RvIiwiaWF0IjoxNTg0MDA2ODkzLCJleHAiOjE1ODQwOTMyOTR9.eN50asSEHBQ4ed2qNvHf6I25KUsETbgtlj8VS6IxN7jEm0T1Z9aGIQiywE6nbe3VFY1u7kAiIFQMUhdCgBUGug";
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        //Un principal de type userDetails
        UserDetails userDetails = new User(playerEntity.getPseudo(), playerEntity.getPassword(), authorities);
        //Un authentication
        AnonymousAuthenticationToken anonymousAuthenticationToken = new AnonymousAuthenticationToken("test", userDetails, authorities );

        ResponseEntity<JwtResponse> responseEntityExpected = ResponseEntity.ok(new JwtResponse(jwt, "toto", authorities));
        when(this.jwtProvider.generateJwtToken(this.authentication)).thenReturn(jwt);
        when(this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword())))
                .thenReturn(anonymousAuthenticationToken);
        ResponseEntity<?> responseEntity = this.authRestAPIs.authenticateUser(loginRequest);

        assertEquals(responseEntity.getStatusCode(), responseEntityExpected.getStatusCode());
        assertEquals(responseEntity.getStatusCodeValue(), responseEntityExpected.getStatusCodeValue());
    }
}
