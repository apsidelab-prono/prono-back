package com.apside.prono.repository;

import com.apside.prono.model.PlayerEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity, Long> {

    Optional<PlayerEntity> findByPseudo(String username);

    Optional<PlayerEntity> findByMail(String email);

    boolean existsPlayerEntitiesByPseudo(String username);

    boolean existsPlayerEntitiesByMail(String username);
}
