package com.apside.prono.modelapi;

import java.util.Date;
import java.util.Set;

import com.apside.prono.model.PlayerEntity;
import com.apside.prono.securityconfig.model.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class  Player {
    private String id;
    private String lastName;
    private String firstName;
    private String mail;
    private Date subscribeDate;
    private String pseudo;
}