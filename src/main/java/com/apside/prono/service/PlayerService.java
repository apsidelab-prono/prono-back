package com.apside.prono.service;

import com.apside.prono.errors.common.EntityNotFoundException;
import com.apside.prono.errors.player.BadRequestCreatePlayerException;
import com.apside.prono.mapper.player.PlayerEntityMapper;
import com.apside.prono.mapper.player.PlayerMapper;
import com.apside.prono.model.PlayerEntity;
import com.apside.prono.modelapi.Player;
import com.apside.prono.repository.PlayerRepository;
import com.apside.prono.securityconfig.model.Role;
import com.apside.prono.securityconfig.services.UserPrinciple;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class PlayerService {
	
    private final PlayerRepository playerRepository;

    private final PasswordEncoder encoder;

    private final Environment env;

    private ResourceBundle bundle = ResourceBundle.getBundle("messagesServicesError");

    @Autowired
    public PlayerService(PlayerRepository playerRepository, PasswordEncoder encoder, Environment env) {
        this.playerRepository = playerRepository;
        this.encoder = encoder;
        this.env = env;
    }

    @Transactional
    public PlayerEntity createPlayer(Player player) {
        if (player != null) {
            if (player.getId() != null) {
                throw new BadRequestCreatePlayerException(this.bundle.getString("new_player_create"));
            }
            return this.playerRepository.save(PlayerEntityMapper.INSTANCE.mapPlayerEntity(player));
        }
        throw new BadRequestCreatePlayerException(this.bundle.getString("new_player_empty"));
    }

    public Player getPlayer(long id) {
        Optional<PlayerEntity> playerEntity = this.playerRepository.findById(id);
        if (!playerEntity.isPresent()) {
            String pattern = this.bundle.getString("player_wrong_id");
            String message = MessageFormat.format(pattern, id);
            throw new EntityNotFoundException(message);
        }
        return PlayerMapper.INSTANCE.mapPlayer(playerEntity.get());
    }

    @Transactional
    public List<Player> getAll() {
        return PlayerMapper.INSTANCE.mapPlayerList(this.playerRepository.findAll());
    }

    @Transactional
    public PlayerEntity update(Player player) {
        if (player != null) {
            Optional<PlayerEntity> optPlayer = this.playerRepository.findById(Long.valueOf(player.getId()));
            if (!optPlayer.isPresent()) {
                String pattern = this.bundle.getString("player_wrong_id");
                String message = MessageFormat.format(pattern, Long.valueOf(player.getId()));
                throw new EntityNotFoundException(message);
            }

            PlayerEntity player1 = optPlayer.get();
            player1.setSubscribeDate(player.getSubscribeDate());
            player1.setMail(player.getMail());
            player1.setLastName(player.getLastName());
            player1.setFirstName(player.getFirstName());
            player1.setFirstName(player.getFirstName());
            this.playerRepository.saveAndFlush(player1);

            return player1;
        }
        throw new EntityNotFoundException(this.bundle.getString("update_player_empty"));
    }

    @Transactional
    public void delete(long id) {
        if (!this.playerRepository.findById(id).isPresent()) {
            String pattern = this.bundle.getString("player_wrong_id");
            String message = MessageFormat.format(pattern, id);
            throw new EntityNotFoundException(message);
        }
        this.playerRepository.deleteById(id);
    }

    public PlayerEntity find(String mail) {
        for (int i = 0; i < this.playerRepository.findAll().size(); i++) {
            if (this.playerRepository.findAll().get(i).getMail().equals(mail))
                return this.playerRepository.findAll().get(i);
        }
        return null;
    }

    @Transactional
    public Player getPlayerByPseudo(String pseudo) {
        Optional<PlayerEntity> playerEntity = this.playerRepository.findByPseudo(pseudo);
        if (!playerEntity.isPresent()) {
            String pattern = this.bundle.getString("player_wrong_pseudo");
            String message = MessageFormat.format(pattern, pseudo);
            throw new EntityNotFoundException(message);
        }
        return PlayerMapper.INSTANCE.mapPlayer(playerEntity.get());
    }

    public boolean existsByUsername(String username) {
        return this.playerRepository.existsPlayerEntitiesByPseudo(username);
    }

    public boolean existsByEmail(String email) {
        return this.playerRepository.existsPlayerEntitiesByMail(email);
    }

    public UserPrinciple getPlayerAuthenticated(String email, String password) {
        Optional<PlayerEntity> optPlayerEntity = this.playerRepository.findByMail(email);
        if (!optPlayerEntity.isPresent()) {
            String pattern = this.bundle.getString("player_wrong_pseudo");
            String message = MessageFormat.format(pattern, email);
            throw new EntityNotFoundException(message);
        }
        PlayerEntity playerEntity = optPlayerEntity.get();
        if (!this.encoder.matches(password, optPlayerEntity.get().getPassword())) {
            String pattern = this.bundle.getString("player_wrong_password");
            String message = MessageFormat.format(pattern, email);
            throw new EntityNotFoundException(message);
        }
        return new UserPrinciple(playerEntity.getId(), playerEntity.getFirstName(), playerEntity.getPseudo(), playerEntity.getMail(), playerEntity.getPassword(), this.getAuthorities(playerEntity.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            Set<Role> roles) {
        List<GrantedAuthority> authorities
                = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName().name()));
        }
        return authorities;
    }
}
