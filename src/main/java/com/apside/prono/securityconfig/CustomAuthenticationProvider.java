package com.apside.prono.securityconfig;

import com.apside.prono.securityconfig.services.UserPrinciple;
import com.apside.prono.service.PlayerService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    PlayerService playerService;

    PasswordEncoder encoder;

    @Autowired
    public CustomAuthenticationProvider(PlayerService playerService, PasswordEncoder encoder) {
        this.playerService = playerService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        Authentication result;
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        UserPrinciple userPrinciple = this.playerService.getPlayerAuthenticated(name, password);

        result = new UsernamePasswordAuthenticationToken(userPrinciple, new ArrayList<>());
        return result;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
